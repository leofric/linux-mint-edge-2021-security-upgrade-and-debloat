# Linux Mint Edge 2021 Security Upgrade and Debloat

This will implement a series of commands to increase Mint security and debloat it.

Mint is already several orders of magnitude greater in security and privacy than an operating system like Windows or OSX. It's defaults are more secure than FreeBSD. 

Mint Edge gives users a more advanced kernel than standard Mint and the packages are up brought up to date to match.

This script will remove anti-privacy Firefox and install Brave from their repository. Comment out those lines if you do not want to install brave. Included in the installation of file is the surf browser, as well as i3 and openbox desktops and several other pro-privacy or productivity apps. 

In addition, this script will install youtube-dl, fuzzy finder and uerbezug. This will allow you to watch and search youtube videos from the commandline (with thumbnails). Watching youtube videos in this way avoids ads, pop-ups and other garbage associated with youtube.com. The basic command to search youtube is ytfzf -t thingyouwanttofind OR you can type ytfzf -t -d thingyouwanttofind to download the video you find. youtube-dl works the same as before and is leveraged by ytfzf.

Take the time to check out the commands and pick and choose, comment out etc. Review the section on installed software and edit accordingly. 

Again this is to boost security. When you are done download cisofy's lynis and audit your security from there.

INSTRUCTIONS:
# place the script in some directory and issue
chmod +x mint2021.sh

# then run
sudo ./mint2021

# there are some additional options in the script which can increase security even further but may break things. currently these are commented out
